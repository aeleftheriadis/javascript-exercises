var request = require('request'),
	cheerio = require('cheerio'),
	url = process.argv.slice(2);
	
if(typeof url[0]!=="undefined"){
	request(url[0], function(error, response, html){
		if(!error){
			var $ = cheerio.load(html),
				rating,
				starsCount=0,
				result;

			$('#left-stack .extra-list.customer-ratings .rating div').last().children().each(function(index, value) {
				if(value.attribs.class === 'rating-star'){
			 		starsCount = starsCount + 1;
				}
				else if(value.attribs.class === 'rating-star half'){
					starsCount = starsCount + 0.5;
				}	
			});

			rating = $('#left-stack .extra-list.customer-ratings .rating .rating-count').last().text().split(" ");

			result = url[0]+ ' has a star rating of '+ starsCount + ' and star ratings count of '+ rating[0] + ' at the time of writing.';
			process.stdout.write('\n' + result + '\n');
		}
		else{
			process.stdout.write('\n' + error + '\n');
			console.log('Error:' + error);
		}
	})
}
else{
	process.stdout.write('\nError: Please provide a valid itunes app url\n');
}

