##Problem 01

 First of all we create a package.json with all the required dependencies.

 We need two packages cherrio to traverse the dom and express to make http calls

 I have developed a node.js console app

 I have included the node_modules so npm install is not needed

 To execute the application, you simply have to open a console window, cd into the folder of this directory and the run node app.js and the url of the itunes app

 Example execute *"node app.js https://itunes.apple.com/us/app/facebook/id284882215?mt=8"*

 If I had more time I would certainly do some unit testing, and pass as input parameter to node.js app the name of the application and not the url.