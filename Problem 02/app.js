  var rsvpTable;

  $(function(){
    //DataTable initialization with ascending order on first 2 columns
    rsvpTable = $('#rsvpTable').DataTable({
      "bSort": true,
      "orderMulti": true,
      "paging": false,
      "bFilter":false,
      "info":false ,
      "order": [[ 0, 'asc' ], [ 1, 'asc' ]],
      responsive: true
    });
  });

  must.Rsvps(function(rsvp) {
    var country,
        city,
        rowData;
    // loop through group topics
    for (key in rsvp.group.group_topics) {
      if(rsvp.group.group_topics[key].topic_name === 'Internet of Things'){

        /** Get country name from countries.js function **/
        country = getCountryName(rsvp.group.group_country.toUpperCase()); 
        city = rsvp.group.group_city;

        /** Get the row indexes if the country and the city is already populated in the table.
        **/
        var indexes = rsvpTable.rows().eq( 0 ).filter( function (rowIdx) {
            return rsvpTable.cell( rowIdx, 0 ).data() === country && rsvpTable.cell( rowIdx, 1 ).data() === city ? true : false;
        } );

        // If row is found we increase the counter and update data
        if(indexes.length>0){
          rowData = rsvpTable.row( indexes ).data();
          rowData[2] =  rowData[2]+1;
          rsvpTable.row( indexes ).data(rowData).draw();
        }
        else{
          //if this city and country is not already populated in the table we add a new row with counter 1
          rsvpTable.row
            .add( 
              [country, city, 1]
            )
            .draw( true );
        }
      } 
    }        
  });