##Problem 02

 I have used the Long­Polling API because web sockets are not well supported on all browsers and doesn't work well behind a proxy.

 I have used https://www.datatables.net/ for the table and https://gist.github.com/maephisto/9228207 for conversion from country code to country name.

 I have opened console, cd into this diresctory and the executed *"bower init"* to initialize an empty application. This command created bower.json

 Then I have added the following bower packages *"bootstrap": "~3.3.5", "datatables": "~1.10.9", "datatables-responsive": "~1.0.7", "must": "~0.1.3"* 

 The application javascript file is app.js and the application can be executed if you open on a browser the index.html file
