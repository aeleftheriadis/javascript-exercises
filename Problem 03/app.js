/**
 * Find the nth prime number
 * @param {Number} n 
 */
function getNthPrime(n) {
    for (var primes = [], i = 2; primes.length < n; i++) {
            for (var root = Math.sqrt(i), j = 0; primes[j] <= root; j++) {
                    if (i%primes[j] === 0) root = 0;
            }
            if (root) primes.push(i);
    }
    return primes[n - 1];
}