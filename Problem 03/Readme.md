##Problem 03

 I have opened console, cd into this diresctory and the executed *"bower init"* to initialize an empty application. This command created bower.json

 Then I wanted to do some unit test of the code to check if everything is ok so I have added qunit with the command bower install --save-dev qunit

 I have created the application in the file named app.js and the qunit test in the file named test.js

 In file app.js I have included one function getNthPrime which takes as parameter the nth prime and returns the value of the nth prime.

 In file test.js you can see the equality checks of the input nth prime and the expected result.

 If you open test.html you will see the results of the qunit tests on the browser.

#Δοκιμαστική διαίρεση
Η πιο βασική μέθοδος ελέγχου αν ένας ακέραιος αριθμός είναι πρώτος ονομάζεται δοκιμαστική διαίρεση. Αυτή η μέθοδος συνίσταται στη διαίρεση του n από κάθε ακέραιο m, ο οποίος είναι μεγαλύτερος του 1 και μικρότερος ή ίσος της τετραγωνικής ρίζας του n. Αν το αποτέλεσμα οποιασδήποτε από αυτές τις διαιρέσεις είναι ακέραιος, τότε ο n δεν είναι πρώτος, ειδάλλως είναι πρώτος αριθμός. Πράγματι, αν n = αb είναι σύνθετος (με α και b ≠ 1), τότε ένας από τους παράγοντες α ή b απαραίτητα το πολύ ίσος με \sqrt{n}. Για παράδειγμα, για n = 37 οι δοκιμαστικές διαιρέσεις είναι από τους m = 2, 3, 4, 5 και 6. Κανένας από αυτούς τους αριθμούς δε διαιρεί το 37, οπότε ο 37 είναι πρώτος. Αυτός ο τρόπος μπορεί να εφαρμοστεί πιο αποτελεσματικά αν μια ολοκληρωμένη λίστα πρώτων αριθμών μέχρι και \sqrt{n} είναι γνωστή - μετά οι δοκιμαστικές διαιρέσεις χρειάζεται να γίνουν μόνο για εκείνους τους m που είναι πρώτοι. Για παράδειγμα, για να ελέγξουμε αν ο 37 είναι πρώτος, μόνο τρεις διαιρέσεις είναι απαραίτητες (m = 2, 3 και 5), αφού οι αριθμοί 4 και 6 είναι σύνθετοι.



#Trial division
The most basic method of checking the primality of a given integer n is called trial division. This routine consists of dividing n by each integer m that is greater than 1 and less than or equal to the square root of n. If the result of any of these divisions is an integer, then n is not a prime, otherwise it is a prime. Indeed, if n=a b is composite (with a and b ≠ 1) then one of the factors a or b is necessarily at most \sqrt{n}. For example, for  n = 37 , the trial divisions are by m = 2, 3, 4, 5, and 6. None of these numbers divides 37, so 37 is prime. This routine can be implemented more efficiently if a complete list of primes up to \sqrt{n} is known—then trial divisions need to be checked only for those m that are prime. For example, to check the primality of 37, only three divisions are necessary (m = 2, 3, and 5), given that 4 and 6 are composite.

While a simple method, trial division quickly becomes impractical for testing large integers because the number of possible factors grows too rapidly as n increases. According to the prime number theorem explained below, the number of prime numbers less than \sqrt{n} is approximately given by \sqrt{n} / \ln(\sqrt{n}), so the algorithm may need up to this number of trial divisions to check the primality of n. For n = 1020, this number is 450 million—too large for many practical applications.