QUnit.test("Primes test", function() {
    equal(getNthPrime(1), 2);
    equal(getNthPrime(2), 3);
    equal(getNthPrime(3), 5);
    equal(getNthPrime(4), 7);
    equal(getNthPrime(5), 11);
    equal(getNthPrime(6), 13);
    equal(getNthPrime(10001), 104743);   
});
